//problem1
function matchesPlayedPerYear(matches) {
    const result1 = {};
    for (let match of matches) {
      const season = match.season;
      if (result1[season]) {
        result1[season] += 1;
      } else {
        result1[season] = 1;
      }
    }
    return result1;
  }

//problem2
function matchesWonPerTeam(matches) {
    let result2 = {};
    matches.forEach((match) => {
        let season = match.season;
        let winner = match.winner;
        if (result2[season]) {
            if (result2[season][winner]) {
                result2[season][winner] += 1;
            } else {
                result2[season][winner] = 1;
            }
        } else {
            result2[season] = {};
            result2[season][winner] = 1;
        }
    })
    return result2;
}

//problem3
function extraRunsPerTeamInYear(matches, deliveries, year) {

    let result3 = {};
  
    matches.forEach(match => {
  
        if (match.season == year) {
  
            deliveries.forEach(delivery => {
  
                if (match.id == delivery.match_id) {
  
                    if (result3[delivery.bowling_team]) {
                        result3[delivery.bowling_team] += Number(delivery.extra_runs);
                    }
                    else {
                        result3[delivery.bowling_team] = Number(delivery.extra_runs);
                    }
                }
            });
  
        }
  
    });
  
    return result3;
  
  }

//problem4
function top10EconomicalBowlersInYear(matches, deliveries, year) {
    let bowlerData = {};
    let topBowlers = [];
    let result4 = {};
    matches.filter(match => match.season == year).forEach(match => {
        deliveries.filter(delivery => match.id == delivery.match_id).forEach(delivery => {
            if (bowlerData[delivery.bowler]) {
                bowlerData[delivery.bowler].balls += 1;
                bowlerData[delivery.bowler].total_runs += Number(delivery.total_runs);
            } else {
                bowlerData[delivery.bowler] = {};
                bowlerData[delivery.bowler].total_runs = Number(delivery.total_runs);
                bowlerData[delivery.bowler].balls = 1;
            }
        });
    });
    for (let bowler in bowlerData) {
        let overs = (bowlerData[bowler].balls) / 6;
        let runs = bowlerData[bowler].total_runs;
        let economyRate = Math.round(runs/overs*100)/100;
        topBowlers.push([bowler, economyRate]);
    }
    topBowlers.sort((a, b) => a[1] - b[1]);
    (topBowlers.slice(0, 10)).forEach(item => {
        result4[item[0]] = item[1];
    });
    return result4;
  }

module.exports = {
    matchesPlayedPerYear,
    matchesWonPerTeam,
    extraRunsPerTeamInYear,
    top10EconomicalBowlersInYear
};