const ipldata = require('./ipl.js');
const csvtojson1 = require('csvtojson');
const csvtojson2 = require('csvtojson');
const csvfilepath1 = '../data/matches.csv'
const csvfilepath2 = '../data/deliveries.csv'
const matches = require('../data/matches.json');
const deliveries = require('../data/deliveries.json');
const fs = require('fs');


csvtojson1()
    .fromFile(csvfilepath1)
    .then((json) => {
        fs.writeFileSync("../data/matches.json", JSON.stringify(json), err => {
            if (err) {
                console.log(err)
            }
        })
    })
csvtojson2()
    .fromFile(csvfilepath2)
    .then((json) => {
        fs.writeFileSync("../data/deliveries.json", JSON.stringify(json), (err) => {
            if (err) {
                console.log(err)
            }
        })
    })



resultp1 = ipldata.matchesPlayedPerYear(matches);
resultp2 = ipldata.matchesWonPerTeam(matches);
resultp3 = ipldata.extraRunsPerTeamInYear(matches, deliveries, 2016);
resultp4 = ipldata.top10EconomicalBowlersInYear(matches, deliveries, 2015);



fs.writeFileSync("../public/output/matchesPlayedPerYear.json", JSON.stringify(resultp1), err => {
    if (err) {
        console.log(err)
    }
})

fs.writeFileSync("../public/output/matchesWonPerTeam.json", JSON.stringify(resultp2), err => {
    if (err) {
        console.log(err)
    }
})

fs.writeFileSync("../public/output/extraRunsPerTeamInYear.json", JSON.stringify(resultp3), err => {
    if (err) {
        console.log(err)
    }
})

fs.writeFileSync("../public/output/top10EconomicalBowlersInYear.json", JSON.stringify(resultp4), err => {
    if (err) {
        console.log(err)
    }
})